// main.js

$(document).ready(function() {

	$('#main-image').click(function() {
		$('#main-image').fadeOut('slow', function() {
			var randomInt = Math.floor((Math.random() * 17)) + 1;
			if(randomInt < 10)
				randomInt = "0" + randomInt;
				 
			var image = "site-images/swicons/" + randomInt + ".png";
			$('#main-image').attr('src', image);

			$(this).fadeIn('slow');
		});
	});

	$('#main-image').click();

});