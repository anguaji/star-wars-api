﻿using System;
using UIKit;
using CoreGraphics;

namespace ThreadTest_ios {
	public class LoadingOverlay : UIView {

		UIActivityIndicatorView activitySpinner;
		UILabel loadingLabel;
		UIProgressView progressBar;

		public LoadingOverlay(CGRect frame) : base(frame) {
			BackgroundColor = UIColor.Black;
			Alpha = 0.85f;
			AutoresizingMask = UIViewAutoresizing.All;

			nfloat labelHeight = 22;
			nfloat labelWidth = Frame.Width - 20;

			nfloat centerX = Frame.Width / 2;
			nfloat centerY = Frame.Height / 2;

			// Activity Spinner
			activitySpinner = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
			activitySpinner.Frame = new CGRect(
				centerX - (activitySpinner.Frame.Width / 2),
				centerY - activitySpinner.Frame.Height - 20,
				activitySpinner.Frame.Width,
				activitySpinner.Frame.Height
			);
			activitySpinner.AutoresizingMask = UIViewAutoresizing.All;
			AddSubview(activitySpinner);
			activitySpinner.StartAnimating();

			// Loading Label
			loadingLabel = new UILabel(new CGRect(
				centerX - (labelWidth / 2),
				centerY - 20,
				labelWidth,
				labelHeight
			));

			loadingLabel.BackgroundColor = UIColor.Clear;
			loadingLabel.TextColor = UIColor.White;
			loadingLabel.Text = "Loading data\n0%";
			loadingLabel.TextAlignment = UITextAlignment.Center;
			loadingLabel.AutoresizingMask = UIViewAutoresizing.All;
			AddSubview(loadingLabel);

			// Progress Bar
			progressBar = new UIProgressView(UIProgressViewStyle.Default);
			progressBar.Frame = new CGRect(
				centerX - ((labelWidth / 2) / 2),
				centerY + 20,
				labelWidth / 2,
				labelHeight
			);
			progressBar.AutoresizingMask = UIViewAutoresizing.All;
			progressBar.Progress = 0f;
			AddSubview(progressBar);

		}

		public void UpdatePercentage(float percent) {
			// Loading Label
			loadingLabel.Text = "Loading data\n" + percent + "%";
			Console.Out.WriteLine("Loading data " + percent + "%");
			progressBar.SetProgress(percent, true);
		}

		public void Hide() {
			UIView.Animate(
				0.5, // duration
				() => { Alpha = 0; },
				() => { RemoveFromSuperview(); }
			);
		}
	}
}