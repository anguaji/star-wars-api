using System;
using System.Threading.Tasks;
using CoreGraphics;
using UIKit;

namespace ThreadTest_ios {
	public partial class LoadingView : UIViewController {

		public int index = 1;
		public int upperRange = 88;

		Character[] characterList;

		LoadingOverlay loadingOverlay;
		CGRect bounds = UIScreen.MainScreen.Bounds;

		public LoadingView(IntPtr handle) : base(handle) { }

		public async override void ViewDidLoad() {
			base.ViewDidLoad();


			txtLoadingDetail.Text = "Loading characters... ";

			characterList = new Character[upperRange + 1];
			characterList = await PreLoadCharacters(1, 10, characterList);

			txtLoadingDetail.Text = "Completed";
			spnLoadingSpinner.Alpha = 0;

			CharacterController characterView = this.Storyboard.InstantiateViewController("CharacterDetail") as CharacterController;

			if (characterView != null) {
				characterView.characterList = characterList;
				characterView.PrefersStatusBarHidden();
				this.NavigationController.PushViewController(characterView, true);
			}

		}

		public async Task<Character[]> PreLoadCharacters(int min, int max, Character[] charas) {

			for (int x = min; x <= max; x++) {
				string url = "http://swapi.co/api/people/" + x + "/";

				try {
					Task<string> apiDelegate = StarWarsApi.GetStringFromApiAsync(url);
					string json = await apiDelegate;
					charas[x] = new Character(x);
					await charas[x].PopulateFromJSON(json);
					charas[x].image = await GiphyHelper.GetImageFromSearch(charas[x].name);

					float pc = (float)x / (float)max;
					prgLoadingProgress.SetProgress(pc, true);
					txtLoadingDetail.Text = "Loaded " + charas[x].name;
				}
				catch (Exception ex) {
					InvokeOnMainThread(() => {
						txtLoadingDetail.Text = "Error loading character " + x;
					});	
					Console.Out.WriteLine("Error: MEH " + ex.ToString());
				}
			}

			return charas;

		}
	}
}