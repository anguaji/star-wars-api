﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace ThreadTest_ios {
	public class Character {

		public int id { get; set; }

		public string name { get; set; }
		public string height { get; set; }
		public string mass { get; set; }
		public string hair_color { get; set; }
		public string skin_color { get; set; }
		public string eye_color { get; set; }
		public string birth_year { get; set; }
		public string gender { get; set; }
		public string homeworld { get; set; }
		public string image { get; set; }
		public List<string> films { get; set; }
		public List<string> species { get; set; }
		public List<string> vehicles { get; set; }
		public List<string> starships { get; set; }
		
		public JObject jsonString { get; set; }
		public bool dataHasBeenSet = false;

		public Character (int _id) {
			// code here
			this.id = _id;
			films = new List<string> ();
			species = new List<string> ();
			vehicles = new List<string> ();
			starships = new List<string> ();
			Console.Out.WriteLine ("New character created for id " + this.id);
		}

		// for funsies
		public Character () : this (0) { }

		private string GetStringFromJSON(JObject jobj, string field) {
			return jobj[field].ToString () == "" ? "[blank]" : jsonString[field].ToString ();
		}

		public async Task PopulateFromJSON (string json) {
			jsonString = JObject.Parse (json);
			
			this.name = GetStringFromJSON(jsonString, "name");
			this.height = GetStringFromJSON(jsonString, "height");
			this.mass = GetStringFromJSON(jsonString, "mass");
			this.hair_color = GetStringFromJSON(jsonString, "hair_color");
			this.skin_color = GetStringFromJSON(jsonString, "skin_color");
			this.eye_color = GetStringFromJSON(jsonString, "eye_color");
			this.birth_year = GetStringFromJSON(jsonString, "birth_year");
			this.gender = GetStringFromJSON(jsonString, "gender");

			string homeworldRaw = await StarWarsApi.GetStringFromApiAsync(jsonString["homeworld"].ToString ());
			this.homeworld = GetStringFromJSON(JObject.Parse (homeworldRaw), "name");//JObject.Parse (homeworldRaw)["name"].ToString () == "" ? "[blank]" : JObject.Parse (homeworldRaw)["name"].ToString ();

			foreach (var d in jsonString["species"].Children ()) {
				string speciesRaw = await StarWarsApi.GetStringFromApiAsync((string)d);
				this.species.Add (JObject.Parse (speciesRaw)["name"].ToString ());
				//Console.Out.WriteLine("Got species for " + this.name);
			}

			if (species.Count < 1)
				species.Add ("** Unknown Species **");

			//return;

			foreach (var d in jsonString["films"].Children ()) {
				string filmsRaw = await StarWarsApi.GetStringFromApiAsync((string)d);
				this.films.Add (JObject.Parse (filmsRaw)["title"].ToString ());
			}

			if (films.Count < 1)
				films.Add ("Unknown films");

			foreach (var d in jsonString["vehicles"].Children ()) {
				string vehiclesRaw = await StarWarsApi.GetStringFromApiAsync((string)d);
				this.vehicles.Add (JObject.Parse (vehiclesRaw)["name"].ToString ());
			}

			if (vehicles.Count < 1)
				vehicles.Add ("Unknown vehicles");

			foreach (var d in jsonString["starships"].Children ()) {
				string starshipsRaw = await StarWarsApi.GetStringFromApiAsync((string)d);
				this.starships.Add (JObject.Parse (starshipsRaw)["name"].ToString ());
			}

			if (starships.Count < 1)
				starships.Add ("Unknown starships");

			Console.Out.WriteLine ("Parsed species, films, vehicles, starships for " + name);

			dataHasBeenSet = true;
		}

		public override string ToString () {

			if (!dataHasBeenSet)
				return "Data not set for character " + id;

			string returnString = string.Format ("** {1} **\n\nID: {0}\nName: {1}\nHeight: {2}\nMass: {3}\nHair Color: {4}\nSkin Color: {5}\nEye Color: {6}\nBirth Year: {7}\nGender: {8}\nHomeworld: {9}\n", id, name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, homeworld);

			// films, species, vehicles, starships

			returnString += "Species:\n";
			foreach (var d in species)
				returnString += "\t" + d + "\n";

			returnString += "Films:\n";
			foreach (var d in films)
				returnString += "\t" + d + "\n";

			returnString += "Vehicles:\n";
			foreach (var d in vehicles)
				returnString += "\t" + d + "\n";

			returnString += "Starships:\n";
			foreach (var d in starships)
				returnString += "\t" + d + "\n";

			return returnString;
		}
	}
}
