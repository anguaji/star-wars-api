// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ThreadTest_ios
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel apiBBY { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel apiGender { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel apiHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel apiHomeworld { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel apiMass { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel apiName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIWebView apiPicture { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel apiSpecies { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnBack { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnNext { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView txtCharacter { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel txtStatus { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (apiBBY != null) {
                apiBBY.Dispose ();
                apiBBY = null;
            }

            if (apiGender != null) {
                apiGender.Dispose ();
                apiGender = null;
            }

            if (apiHeight != null) {
                apiHeight.Dispose ();
                apiHeight = null;
            }

            if (apiHomeworld != null) {
                apiHomeworld.Dispose ();
                apiHomeworld = null;
            }

            if (apiMass != null) {
                apiMass.Dispose ();
                apiMass = null;
            }

            if (apiName != null) {
                apiName.Dispose ();
                apiName = null;
            }

            if (apiPicture != null) {
                apiPicture.Dispose ();
                apiPicture = null;
            }

            if (apiSpecies != null) {
                apiSpecies.Dispose ();
                apiSpecies = null;
            }

            if (btnBack != null) {
                btnBack.Dispose ();
                btnBack = null;
            }

            if (btnNext != null) {
                btnNext.Dispose ();
                btnNext = null;
            }

            if (txtCharacter != null) {
                txtCharacter.Dispose ();
                txtCharacter = null;
            }

            if (txtStatus != null) {
                txtStatus.Dispose ();
                txtStatus = null;
            }
        }
    }
}