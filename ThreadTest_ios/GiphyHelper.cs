﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;

namespace ThreadTest_ios {
	public static class GiphyHelper {
		private const string apiKey = "dc6zaTOxFJmzC";
		private const string extraSearch = "Star Wars";

		public static async Task<string> GetImageFromSearch(string search) {
			search += " " + extraSearch;
			string url = String.Format("http://api.giphy.com/v1/gifs/search?q={0}&api_key={1}", HttpUtility.UrlEncode(search), apiKey);

			using (var httpClient = CreateClient()) {
				var response = await httpClient.GetAsync(url).ConfigureAwait(false);
				if (response.IsSuccessStatusCode) {
					var contents = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

					if (!string.IsNullOrWhiteSpace(contents))
						return JObject.Parse(contents)["data"][0]["images"]["fixed_height_small"]["url"].ToString();
					else
						return string.Format("Response from {0} empty", url);

				} else return response.StatusCode.ToString();
			}
		}

		public static HttpClient CreateClient() {
			var httpClient = new HttpClient();

			httpClient.DefaultRequestHeaders.Accept.Clear();
			httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			return httpClient;
		}
	}
}