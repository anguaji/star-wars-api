// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ThreadTest_ios
{
    [Register ("LoadingView")]
    partial class LoadingView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIProgressView prgLoadingProgress { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIActivityIndicatorView spnLoadingSpinner { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel txtLoadingDetail { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (prgLoadingProgress != null) {
                prgLoadingProgress.Dispose ();
                prgLoadingProgress = null;
            }

            if (spnLoadingSpinner != null) {
                spnLoadingSpinner.Dispose ();
                spnLoadingSpinner = null;
            }

            if (txtLoadingDetail != null) {
                txtLoadingDetail.Dispose ();
                txtLoadingDetail = null;
            }
        }
    }
}