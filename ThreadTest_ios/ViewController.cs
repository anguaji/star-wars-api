﻿using System;
using System.IO;
using System.Threading.Tasks;
using CoreGraphics;
using Foundation;
using UIKit;

namespace ThreadTest_ios {
	public partial class ViewController : UIViewController {
		protected ViewController(IntPtr handle) : base(handle) {
			// Note: this .ctor should not contain any initialization logic.
		}

		public int index = 1;
		public int upperRange = 88;

		Random rand = new Random();

		Character myCharacter = new Character();
		Character[] characterList;

		LoadingOverlay loadingOverlay;
		CGRect bounds = UIScreen.MainScreen.Bounds;

		private int GetRandomInt() {
			return rand.Next(1, upperRange);
		}

		public async override void ViewDidLoad() {
			base.ViewDidLoad();

			if(characterList != null)
				characterList = new Character[upperRange + 1];

			Console.Out.WriteLine("View loaded with " + characterList.Length + " items");

			await PreLoadCharacters (1, 6);

			//RetrieveCharacterInfo (1, btnNext);

			btnNext.TouchUpInside += (object Sender, EventArgs e) => {
				btnNext.Enabled = false;
				index++;
				if (index > upperRange)
					index = 1;
				RetrieveCharacterInfo(index, Sender);
			};

			btnBack.TouchUpInside += (object Sender, EventArgs e) => {
				btnBack.Enabled = false;
				index--;
				if (index < 1)
					index = upperRange;
				RetrieveCharacterInfo(index, Sender);
			};
		}

		void CharactersLoadedEvent () {
			Console.Out.WriteLine ("LOADED ALL CHARACTERS, retrieving character 1");
			RetrieveCharacterInfo (1, btnNext);
		}

		async Task RetrieveCharacterInfo(int randomInt, object Sender) {
			
			txtCharacter.Text = "";

			if (characterList[randomInt] == null) {
				Console.Out.WriteLine("Character has not yet been set. Setting...");

				Task<bool> LoadChara = LoadCharacter(randomInt);
				bool didLoad = await LoadChara;

				if (!didLoad) {
					if (Sender == btnNext) {
						index = randomInt + 1;
						if (index > upperRange)
							index = 1;
					} else {
						index = randomInt - 1;
						if (index < 1)
							index = upperRange;
					}
						
					Console.Out.WriteLine ("Something went wrong, changing index to " + index + " and reloading");
					RetrieveCharacterInfo (index, Sender);
					return;
				} else {
					Console.Out.WriteLine ("Created character " + characterList[randomInt].name + " at " + randomInt);
				}

			} else {
				Console.Out.WriteLine("Character set from cache: " + myCharacter.name);
			}

			myCharacter = characterList[randomInt];

			btnBack.Enabled = true;
			btnNext.Enabled = true;

			UpdateView(myCharacter);

		}

		void UpdateView(Character viewCharacter) {
			txtCharacter.Text = viewCharacter.ToString();

			apiName.Text = viewCharacter.name;
			apiHomeworld.Text = viewCharacter.homeworld;

			//string speciesList = string.Empty;
			//speciesList = String.Join(", ", viewCharacter.species);

			if (viewCharacter.species.Count > 0)
				apiSpecies.Text = String.Join(", ", viewCharacter.species); //viewCharacter.species[0];
			else
				apiSpecies.Text = "[species not set]";
			
			string contentDirectoryPath = Path.Combine(NSBundle.MainBundle.BundlePath, "Content/");
			string localHtmlUrl = Path.Combine(contentDirectoryPath, "index.html");
			apiPicture.LoadRequest(new NSUrlRequest(new NSUrl(localHtmlUrl, false)));
			apiPicture.ScalesPageToFit = false;

			apiGender.Text = viewCharacter.gender;
			apiMass.Text = viewCharacter.mass;
			apiHeight.Text = viewCharacter.height;
			apiBBY.Text = viewCharacter.birth_year;
		}

		public async Task PreLoadCharacters (int min, int max) {
			loadingOverlay = new LoadingOverlay (bounds);
			View.Add (loadingOverlay);

			for (int x = min; x <= max; x++) {
				string url = "http://swapi.co/api/people/" + x + "/";

				try {
					Task<string> apiDelegate = StarWarsApi.GetStringFromApiAsync (url);
					string json = await apiDelegate;
					characterList[x] = new Character (x);
					await characterList[x].PopulateFromJSON (json);
					Console.Out.WriteLine ("Populated " + characterList[x].name);

					float pc = (float)x / (float)max;
					loadingOverlay.UpdatePercentage (pc);
					Console.Out.WriteLine ("Loaded " + characterList[x].name);
				}
				catch (Exception ex) {
					Console.Out.WriteLine ("Error: MEH "  + ex.ToString());
				}
			}

			loadingOverlay.Hide ();

			CharactersLoadedEvent ();

		}

		public async Task<bool> LoadCharacter (int index) {
			bool didLoad = false;
			loadingOverlay = new LoadingOverlay (bounds);
			View.Add (loadingOverlay);

			string url = "http://swapi.co/api/people/" + index + "/";

			try {
				Task<string> apiDelegate = StarWarsApi.GetStringFromApiAsync (url);
				string json = await apiDelegate;
				Console.Out.WriteLine ("Got data, setting character at index " + index);
				characterList[index] = new Character (index);
				await characterList[index].PopulateFromJSON (json);
				Console.Out.WriteLine("JSON Populated: " + index);
				float pc = 0.5f;
				loadingOverlay.UpdatePercentage (pc);
				didLoad = true;
			}
			catch (Exception ex) {
				Console.Out.WriteLine ("Error: MEH ");
				didLoad = false;
			}

			loadingOverlay.Hide ();
			return didLoad;
		}

		public override void DidReceiveMemoryWarning() {
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		public void UpdateStatus(string content) {
			txtStatus.Text = content;
		}
	}
}

