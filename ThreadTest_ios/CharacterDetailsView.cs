using Foundation;
using System;
using UIKit;

namespace ThreadTest_ios
{
    public partial class CharacterDetailsView : UIViewController
    {
        public CharacterDetailsView (IntPtr handle) : base (handle) { }

		public string details;

		public override void ViewDidLoad() {
			base.ViewDidLoad();

			txtDetails.Text = details;
			txtDetails.Font = UIFont.FromName("Slabo13px-Regular", 14f);
			btnModalDone.TouchUpInside += (object Sender, EventArgs e) => {
				DismissModalViewController(true);
			};
		}
    }
}