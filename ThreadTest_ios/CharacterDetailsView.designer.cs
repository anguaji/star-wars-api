// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ThreadTest_ios
{
    [Register ("CharacterDetailsView")]
    partial class CharacterDetailsView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnModalDone { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView txtDetails { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btnModalDone != null) {
                btnModalDone.Dispose ();
                btnModalDone = null;
            }

            if (txtDetails != null) {
                txtDetails.Dispose ();
                txtDetails = null;
            }
        }
    }
}