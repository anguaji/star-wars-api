﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.IO;

namespace ThreadTest_ios {
	public class StarWarsApi {
		public static async Task<string> GetStringFromApiAsync(string url) {
			var httpClient = new HttpClient();
			string contents = await httpClient.GetStringAsync(url);
			httpClient.Dispose();
			System.Console.WriteLine ("Retrieved: " + contents);
			return contents;
		}

		public static string GetStringFromApi(string url) {
			var request = HttpWebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "get";
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			StreamReader reader = new StreamReader(response.GetResponseStream());
			return reader.ReadToEnd();
		}

	}

}