using Foundation;
using System;
using UIKit;

namespace ThreadTest_ios
{
    public partial class CharacterListTable : UITableViewController
    {
        public CharacterListTable (IntPtr handle) : base (handle) { }

		String[] TableItems;
		string CellIdentifier = "TableCell";

		public Character[] characterList;

		public override void ViewDidLoad() {
			base.ViewDidLoad();
		}

		public void TableSource(string[] items) {
			TableItems = items;
		}

		public override nint RowsInSection(UITableView tableView, nint section) {
			return TableItems.Length;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath) {
			UITableViewCell cell = tableView.DequeueReusableCell(CellIdentifier);
			string item = TableItems[indexPath.Row];

			if(cell == null) {
				cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier);
			}

			cell.TextLabel.Text = item;

			return cell;
		}
    }
}